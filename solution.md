# solution

# question 1
```Shell
hdfs dfs -mkdir /user/
hdfs dfs -mkdir /user/root

hdfs dfs -copyFromLocal /opt/hadoop/etc/hadoop/ /user/root/input
hadoop jar /opt/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.1.jar grep /user/root/input/hadoop-env.cmd /user/root/output 'dfs[a-z.]+'
hdfs dfs -ls /user/root/output
hdfs dfs -cat /user/root/output/part-r-00000

hdfs dfs -copyToLocal /user/root/output ./output
cat ./output/part-r-00000
```

## question 2
```Shell

hadoop jar /opt/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.1.jar grep /user/root/input/hadoop-env.cmd /user/root/output2 'h[a-z.]+'
hdfs dfs -copyToLocal /user/root/output2 ./output2
cat ./output2/part-r-00000
```

## question 3
```Shell
hdfs dfs -copyFromLocal ./data/verlaine.txt /user/root/verlaine.txt
hadoop jar /opt/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.3.1.jar wordcount /user/root/verlaine.txt /user/root/output3
hdfs dfs -copyToLocal /user/root/output3 ./output3
cat ./output3/part-r-00000
```


## question 4
idem 

## question 5
See the [mapreduce turorial](https://hadoop.apache.org/docs/r3.3.1/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html)

```Shell
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=${JAVA_HOME}/bin:${PATH}
export HADOOP_CLASSPATH=${JAVA_HOME}/lib/tools.jar

cd src
hadoop com.sun.tools.javac.Main WordCount.java
jar cf wc.jar WordCount*.class
cd ..
hdfs dfs -copyFromLocal /data/verlaine.txt /verlaine.txt
hadoop jar src/wc.jar WordCount /verlaine.txt /output
hdfs dfs -ls /output
hdfs dfs -cat /output/part-r-00000
```

## question 6
