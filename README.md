# hadoop

# D'abord un peu d'introduction / théorie / rappel:

Vers le [ppt](https://1drv.ms/p/s!AjTdBgChq01ohBooCQmRru9_TxO5?e=zstBvc)

## Installation

Soit vous êtes connectés à GIT, sinon, ajoutez votre clé:

![Where to find ssh menu](doc/images/ssh-menu.png)

Puis:

![Saisissez votre clé](doc/images/ssh-keys.png)

Et, installez la clé en suivant les instructions décrites [ici](https://gitlab.isima.fr/help/ssh/index.md#generate-an-ssh-key-pair).

Créez un répertoire de travail pour le TP et lancez:

```shell
git clone git@gitlab.isima.fr:iia_ancaumon/hadoop.git
```


## Construire
* La construction et l'exécution de l'image sont automatisées par:
``` shell
bin/run.sh
```
* Pour entrer dans le container, faites:
``` shell
start-hadoop.sh
```

* Et ouvez un navigateur vers: [`localhost:8088`](localhost:8088).


# Exercices:

L'objectif de ces exercices est de faire un peu de configuration de mapreduce, afin de se familiariser avec l'environnement, en simulant plusieurs noeuds, mais en restant en local. D'abord, nous utiliserons des mapreduce déjà fait, puis nous écrirons le notre.

* Pour l'ensemble des questions suivantes, lisez la [documentation d'apache](https://hadoop.apache.org/docs/stable/). Elle est un peu ardue, des pointeurs particuliers sont fournis dans les questions. Commencez par vous familiarisez avec elle.
* Chaque `bin/run.sh` redémarre le container, from scratch, et efface votre contenu
* Quitter le docker ne l'arrête pas, 
* Si vous avez besoin d'un connecteur à hadoop, faites `bin/connect.sh`.
* L'installation d'hadoop est réalisée dans le container docker dans le répertoire `/opt/hadoop`, toutes les références à hadoop sont contextualisés dans ce répertoire, ajouté dont le bin est ajouté dans le `$PATH` pour plus de facilité.

## Question 1 - grep sur des fichiers exemples.

L' objectif est de copier des fichiers sur dfs, faire tourner un mapreduce d'exemple (grep), et récupérer les résultats. Apprendre à faire l'aller et retour avec le cluster, en somme !

* Construisez le répertoire de base de root `/user/root` sur le noeud comme le suggère la [documentation d'apache](https://hadoop.apache.org/docs/r2.4.1/hadoop-project-dist/hadoop-hdfs/HdfsDesign.html).

* Copiez les fichiers de configuration depuis `etc/hadoop` dans hdfs, en suivant la [documentation](https://hadoop.apache.org/docs/r2.4.1/hadoop-project-dist/hadoop-common/FileSystemShell.html)

* Dans la page de setup, il y a un exemple sur un mapreduce fournit par hadoop, [le grep](https://hadoop.apache.org/docs/r2.4.1/hadoop-project-dist/hadoop-common/SingleCluster.html), appliquez cet exemple au fichier `hadoop-env.cmd`, copié précédémment

* Regardez le résultat directement dans le dfs

* Récupérez localement les fichiers depuis le cluster pour les examiner

## Question 2 - on prend de l'assurance sur une variante

* Comptez les occurences des mots commençant par h dans le fichier `hadoop-env.cmd`

## Question 3 - exécution d'un exemple différent

* Transférez le fichier `data/verlaine.txt` sur le dfs

* Exécutez le comptage de mots, c'est un des exemples d'`hadoop-mapredurce-examples`, il suffit d'indiquer `wordcount` au lieu de `grep` et l'exemple tourne.

## Question 4 - en sortant du docker maintenant

* Hadoop tourne dans une image docker, ces images ne sont pas persistantes, tous les fichiers qui y sont décrits disparaissent à la mort du conteneur.

* Le répertoire `data` a été monté comme un volume, son contenu à l'intérieur du docker `/data` et à l'extérieur du docker `./docker` sont identiques à chaque instant,

* Essayé de créer un fichier d'un côté, observez de l'autre

* Télécharger un gros fichier texte et faites tourner le wordcount.


## Question 5 - faites vous même un mapreduce AOP
Suivez la documentation pour [réaliser mapreduce ](https://hadoop.apache.org/docs/r3.3.1/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html) dans un jar

## Question 6 - Parser vous même un fichier

Le fichier `data/dates.txt` est au format suivant: un prénom, une année, et des valeurs. L'objectif est de calculer la moyenne, globale, par année (quel que soit le prénom, et moyenne de toutes les valeurs en colonnes).

* Ecrivez une classe `MapperDates` qui implémente `MapReduceBase` et étends `Mapper`

   * Renseignez vous sur `Mapper` dans la [doc](h* et sur `MapReduceBase` dans la [doc](https://hadoop.apache.org/docs/current/api/org/apache/hadoop/mapred/MapReduceBase.html).

   * qui doit implémenter `Mapper<LongWritable Text Text IntWritable`

* De même, une classe `ReducerDates` qui implémente `Reducer< Text, IntWritable, Text, IntWritable >`, cf. la [documentation](https://hadoop.apache.org/docs/current/api/org/apache/hadoop/mapred/Reducer.html).
