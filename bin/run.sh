#!/usr/bin/env bash

echo "Stop container"
docker stop my-hadoop-container
echo "Remove container"
docker rm my-hadoop-container
echo "Build container"
docker build -t my-hadoop .
echo "Run container"
docker run -p 8088:8088 -v $PWD/data:/data -v $PWD/src:/src --name my-hadoop-container -d my-hadoop
echo "Enter container"
docker exec -it my-hadoop-container bash
