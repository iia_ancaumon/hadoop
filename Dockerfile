FROM ubuntu:16.04

# Set environment vars
ENV HADOOP_HOME /opt/hadoop
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

# Install packages
RUN \
  apt-get update && apt-get install -y \
  ssh \
  rsync \
  vim \
  curl \
  openjdk-8-jdk


# Download and extract hadoop, set JAVA_HOME in hadoop-env.sh, update path
RUN curl https://dlcdn.apache.org/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz -o hadoop-3.3.1.tar.gz
RUN tar -xzf hadoop-3.3.1.tar.gz && \
   mv hadoop-3.3.1 $HADOOP_HOME && \
   echo "export JAVA_HOME=$JAVA_HOME" >> $HADOOP_HOME/etc/hadoop/hadoop-env.sh && \
   echo "PATH=$PATH:$HADOOP_HOME/bin\n" >> ~/.bashrc && \
   echo "export HDFS_NAMENODE_USER='root'" >> ~/.bashrc && \
   echo "export HDFS_DATANODE_USER='root'" >> ~/.bashrc && \
   echo "export HDFS_SECONDARYNAMENODE_USER='root'" >> ~/.bashrc && \
   echo "export YARN_RESOURCEMANAGER_USER='root'" >> ~/.bashrc && \
   echo "export YARN_NODEMANAGER_USER='root'" >> ~/.bashrc

# Create ssh keys
RUN \
  ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa && \
  cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys && \
  chmod 0600 ~/.ssh/authorized_keys

# Copy hadoop configs
ADD hadoop.conf/*xml $HADOOP_HOME/etc/hadoop/

# Copy ssh config
ADD hadoop.conf/ssh_config /root/.ssh/config

# Copy scripts to start hadoop
ADD hadoop.conf/start-hadoop.sh start-hadoop.sh

# Expose various ports
EXPOSE 8088 50070 50075 50030 50060 9870

# Start hadoop
CMD bash start-hadoop.sh
